package me.rrama.TDM.Events;

import me.rrama.TDM.TDM;
import me.rrama.RramaGaming.Gamer;
import me.rrama.RramaGaming.RramaGaming;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public class ClassChange implements Listener {
    
    @SuppressWarnings(value = "deprecation")
    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerInteract(PlayerInteractEvent event) {
        if (!(event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK)) return;
        if (!RramaGaming.gameInPlay.equals("TeamDeathMatch")) return;
        try {
            if (event.getItem().getTypeId() != 351) return;
        } catch (NullPointerException ex) { return; } //Empty is null not 0.
        Player P = event.getPlayer();
        String PN = P.getName();
        Gamer G = RramaGaming.getGamer(PN);
        if (!G.m()) return;
        String CN = TDM.ClassesUpgradeItem.get(event.getItem().getData());
        if (CN == null) return;
        event.setCancelled(true);
        G.addMetaData("TeamDeathMatch-Class", CN);
        G.addMetaData("TeamDeathMatch-Upgrade", ((int)G.getMetaData("TeamDeathMatch-Upgrade"))+1);
        P.getInventory().remove(351);
        P.updateInventory(); //Bukkit need to get their shoes on and fix this.
        P.sendMessage(ChatColor.GOLD + "Next time you spawn you'll be a " + CN + ".");
    }
}