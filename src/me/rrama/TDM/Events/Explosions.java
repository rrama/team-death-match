package me.rrama.TDM.Events;

import java.util.ArrayList;
import me.rrama.TDM.TDM;
import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.ProjectileHitEvent;

public class Explosions implements Listener {
    
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onEntityExplode(EntityExplodeEvent event) {
        event.setYield(0);
        final ArrayList<BlockState> States = new ArrayList<>();
        for (Block B : event.blockList()) {
            States.add(B.getState());
        }
        Bukkit.getScheduler().runTaskLater(TDM.This, new Runnable() {

            @Override
            public void run() {
                for (BlockState BS : States) {
                    BS.update(true);
                }
            }
        }, 2);
    }
    
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onProjectileHit(ProjectileHitEvent event) {
        Entity e = event.getEntity();
        if (e.getType() == EntityType.EGG) {
            e.getLocation().getWorld().createExplosion(e.getLocation(), 3);
        }
    }
}