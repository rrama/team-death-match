package me.rrama.TDM.Events;

import me.rrama.TDM.TDM;
import org.bukkit.Bukkit;
import org.bukkit.event.Listener;

public class RegisterEvents {
    
    public static void Register () {
        reg(new ClassChange());
        reg(new DeathAndRespawnEvent());
        reg(new Explosions());
        reg(new GainExp());
        reg(new JoinAndQuit1());
        reg(new NoLittering());
        reg(new Pain());
    }
    
    public static void reg(Listener ll) {
        Bukkit.getServer().getPluginManager().registerEvents(ll, TDM.This);
    }
    
}
