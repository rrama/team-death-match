package me.rrama.TDM.Events;

import java.util.Random;
import me.rrama.TDM.TDM;
import me.rrama.TDM.TDMClass;
import me.rrama.TDM.Team;
import me.rrama.RramaGaming.Gamer;
import me.rrama.RramaGaming.RoundState;
import me.rrama.RramaGaming.RramaGaming;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class JoinAndQuit1 implements Listener {

    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerJoin(PlayerJoinEvent event) {
        
        Player P = event.getPlayer();
        String PN = P.getName();
        Gamer G = RramaGaming.getGamer(PN);
        G.addMetaData("TeamDeathMatch-Class", TDM.DefaultClass);
        
        if (!RramaGaming.gameInPlay.equals("TeamDeathMatch")) return;
        
        if (RramaGaming.R == RoundState.InGame) {
            P.setGameMode(GameMode.ADVENTURE);
            Team ChosenTeam;
            try {
                ChosenTeam = TDM.getTeam(PN);
                ChosenTeam.BetratyingMembers.remove(PN);
            } catch (NullPointerException ex) {
                if (TDM.Red.Members.size() < TDM.Blue.Members.size()) {
                    ChosenTeam = TDM.Red;
                } else if (TDM.Blue.Members.size() < TDM.Red.Members.size()) {
                    ChosenTeam = TDM.Blue;
                } else if (TDM.Red.getScore() <  TDM.Blue.getScore()) {
                    ChosenTeam = TDM.Red;
                } else if (TDM.Blue.getScore() <  TDM.Red.getScore()) {
                    ChosenTeam = TDM.Blue;
                } else if (TDM.Red.BetratyingMembers.size() < TDM.Blue.BetratyingMembers.size()) {
                    ChosenTeam = TDM.Red;
                } else if (TDM.Blue.BetratyingMembers.size() < TDM.Red.BetratyingMembers.size()) {
                    ChosenTeam = TDM.Blue;
                } else {
                    if ((new Random()).nextInt(1) == 0) {
                        ChosenTeam = TDM.Red;
                    } else {
                        ChosenTeam = TDM.Blue;
                    }
                }
            }
            ChosenTeam.Members.add(PN);
            G.setTagColour(ChosenTeam.getTeamColour());
            TDMClass C = TDM.Classes.get((String) G.getMetaData("TeamDeathMatch-Class"));
            G.addMetaData("TeamDeathMatch-Upgrade", 0);
            C.setPlayerInvertory(P);
            event.setJoinMessage(ChosenTeam.getTeamColour() + PN + " joined the game.");
            P.teleport(DeathAndRespawnEvent.Spawning());
        }
    }
    
    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerQuit(PlayerQuitEvent event) {

        Player P = event.getPlayer();
        String PN = P.getName();
        try {
            Team team = TDM.getTeam(PN);

            team.Members.remove(PN);
            team.BetratyingMembers.add(PN);
            event.setQuitMessage(team.getTeamColour() + PN + " left the game.");
        } catch (NullPointerException ex) {}
    }
}