package me.rrama.TDM.Events;

import me.rrama.RramaGaming.Gamer;
import me.rrama.RramaGaming.RoundState;
import me.rrama.RramaGaming.RramaGaming;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;

public class NoLittering implements Listener {
    
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerDropItem(PlayerDropItemEvent event) {
        Check(event.getPlayer(), event);
    }
    
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerPickupItem(PlayerPickupItemEvent event) {
        Check(event.getPlayer(), event);
    }
    
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onInventoryOpen(InventoryOpenEvent event) {
        if (event.getPlayer() instanceof Player) {
            Player P = (Player)event.getPlayer();
            if (event.getInventory().getType() != InventoryType.PLAYER && P.getGameMode() != GameMode.CREATIVE) {
                Check(P, event);
            }
        }
    }
    
    void Check(Player P, Cancellable C) {
        if (RramaGaming.gameInPlay.equals("TeamDeathMatch") && RramaGaming.R != RoundState.NeedMorePlayers) {
            Gamer G = RramaGaming.getGamer(P.getName());
            if (G.m()) {
                C.setCancelled(true);
            }
        }
    }
}
