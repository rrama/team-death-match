package me.rrama.TDM.Events;

import java.util.ArrayList;
import me.rrama.TDM.TDM;
import me.rrama.TDM.TDMClass;
import java.util.HashMap;
import java.util.Random;
import me.rrama.RramaGaming.Gamer;
import me.rrama.RramaGaming.RramaGaming;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

public class DeathAndRespawnEvent implements Listener {
    
    public static HashMap<String, Integer> Kills = new HashMap<>();
    
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerDeath(PlayerDeathEvent event) {
        Player P = event.getEntity();
        String PN = P.getName();
        if (RramaGaming.gameInPlay.equals("TeamDeathMatch") && RramaGaming.getGamer(PN).m()) {
            event.setDroppedExp(0);
            event.getDrops().clear();
            Player Attacker = null;
            for (String S : TDM.Red.Members) {
                if (event.getDeathMessage().contains(" " + S)) {
                    TDM.Red.ScoreAdd1();
                    Attacker = Bukkit.getPlayer(S);
                }
                event.setDeathMessage(event.getDeathMessage().replaceAll(S, ChatColor.RED + S + ChatColor.GRAY));
            }
            for (String S : TDM.Blue.Members) {
                if (event.getDeathMessage().contains(" " + S)) {
                    TDM.Blue.ScoreAdd1();
                    Attacker = Bukkit.getPlayer(S);
                }
                event.setDeathMessage(event.getDeathMessage().replaceAll(S, ChatColor.BLUE + S + ChatColor.GRAY));
            }
            if (Attacker != null) {
                int PKills = 1;
                if (DeathAndRespawnEvent.Kills.containsKey(Attacker.getName())) {
                    PKills += DeathAndRespawnEvent.Kills.get(Attacker.getName());
                } 
                DeathAndRespawnEvent.Kills.put(Attacker.getName(), PKills);
                if (DeathAndRespawnEvent.Kills.containsKey(PN)) {
                    P.setTotalExperience(Kills.get(PN));
                    DeathAndRespawnEvent.Kills.remove(PN);
                } else {
                    P.setTotalExperience(0);
                }
                KillStreak(Attacker, PKills);
            }
        }
    }
    
    public static void KillStreak(Player P, int Kills) {
        String PN = P.getName();
        if (Kills == 5) {
            Bukkit.broadcastMessage(TDM.This.TagNo + ChatColor.DARK_PURPLE + PN + " is on a 5 kill streak.");
        } else if (Kills == 7) {
            Bukkit.broadcastMessage(TDM.This.TagNo + ChatColor.DARK_PURPLE + PN + " is on a 7 kill rampage!");
        } else if (Kills == 10) {
            Bukkit.broadcastMessage(TDM.This.TagNo + ChatColor.DARK_PURPLE + PN + " is dominating on a 10 kill rampage!");
        } else if (Kills == 15) {
            Bukkit.broadcastMessage(TDM.This.TagNo + ChatColor.DARK_PURPLE + PN + " must be using hacks, they're on a 15 kill rampage!");
        } else if (Kills > 19 && Kills%5 == 0) {
            if (TDM.getTeam(PN) == TDM.Red) {
                Bukkit.broadcastMessage(TDM.This.TagNo + ChatColor.DARK_PURPLE + "Give up " + ChatColor.DARK_BLUE + "Blue Team " + ChatColor.DARK_PURPLE + PN + " is on a " + Kills + " kill rampage!");
            } else {
                Bukkit.broadcastMessage(TDM.This.TagNo + ChatColor.DARK_PURPLE + "Give up " + ChatColor.DARK_RED + "Red Team " + ChatColor.DARK_PURPLE + PN + " is on a " + Kills + " kill rampage!");
            }
        }
    }
    
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerRespawn(PlayerRespawnEvent event) {
        if (RramaGaming.gameInPlay.equals("TeamDeathMatch") && RramaGaming.getGamer(event.getPlayer().getName()).m()) {
            final Player P = event.getPlayer();
            String PN = P.getName();
            event.setRespawnLocation(Spawning());
            Gamer G = RramaGaming.getGamer(PN);
            TDMClass C = TDM.Classes.get((String)G.getMetaData("TeamDeathMatch-Class"));
            C.setPlayerInvertory(P);
            Bukkit.getScheduler().runTaskLater(TDM.This, new Runnable() {

                @Override
                public void run() {
                    P.setFoodLevel(7);
                }
                
            }, 20);
        }
    }
    
    public static Location Spawning() {
        ArrayList<Location> Spawns = (ArrayList<Location>)RramaGaming.MapInPlay.getMetaData("TeamDeathMatch-Spawns");
        Random R = new Random();
        int i = R.nextInt(Spawns.size());
        return Spawns.get(i);
    }
}
