package me.rrama.TDM.Events;

import java.util.HashMap;
import me.rrama.TDM.TDM;
import me.rrama.TDM.Team;
import me.rrama.RramaGaming.RoundState;
import me.rrama.RramaGaming.RramaGaming;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.ThrownPotion;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.PotionSplashEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Pain implements Listener {
    
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onEntityDamage(EntityDamageEvent event) {
        if (!(RramaGaming.gameInPlay.equals("TeamDeathMatch") && RramaGaming.R == RoundState.InGame)) return;
        if (!(event.getEntity() instanceof Player)) return;
        Player Victim = (Player) event.getEntity();
        DamageCause DC = event.getCause();
        if (DC == DamageCause.FIRE) {
            event.setCancelled(false);
            Victim.setFireTicks(40);
        } else if (DC == DamageCause.FIRE_TICK || DC == DamageCause.ENTITY_EXPLOSION || DC == DamageCause.POISON
                || DC == DamageCause.MAGIC || DC == DamageCause.ENTITY_EXPLOSION) {
            event.setCancelled(false);
        }
    }
    
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onEntityDamageByEntity(EntityDamageByEntityEvent event) {
        if (!(RramaGaming.gameInPlay.equals("TeamDeathMatch") && RramaGaming.R == RoundState.InGame)) return;
        if (!(event.getEntity() instanceof Player)) return;
        Player Victim = (Player) event.getEntity();
        Entity e = event.getDamager();
        if (e instanceof Projectile) {
            e = ((Projectile)e).getShooter();
        }
        if (e instanceof Player) {
            Player Attacker = (Player) e;
            try {
                if (TDM.getTeam(Victim.getName()).equals(TDM.getTeam(Attacker.getName()))) {
                    event.setCancelled(true);
                    Attacker.sendMessage(ChatColor.GOLD + Victim.getName() + " is on your team!");
                } else {
                    event.setCancelled(false);
                }
            } catch (NullPointerException ex) {
                event.setCancelled(true);
                Attacker.sendMessage(ChatColor.GOLD + Victim.getName() + " or yourself are on an invalid team.");
            }
        }
    }
    
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPotionSplash(PotionSplashEvent event) {
        if (!(RramaGaming.gameInPlay.equals("TeamDeathMatch") && RramaGaming.R == RoundState.InGame)) return;
        if (!(event.getPotion().getShooter() instanceof Player)) return;
        ThrownPotion pot = event.getPotion();
        PotionEffect PE = pot.getEffects().iterator().next();
        PotionEffectType PET = PE.getType();
        String PETN = PET.getName();
        boolean good = true;
        if (PETN.equals("BLINDNESS") || PETN.equals("CONFUSION") || PETN.equals("HARM") || PETN.equals("HUNGER")
                || PETN.equals("POISON") || PETN.equals("SLOW") || PETN.equals("WEAKNESS") || PETN.equals("WITHER")) {
            good = false;
        }
        Player P = (Player) event.getPotion().getShooter();
        try {
            Team T = TDM.getTeam(P.getName());
            boolean Dirty = false;
            HashMap<LivingEntity, Double> HMLED = new HashMap<>();
            for (LivingEntity LE : event.getAffectedEntities()) {
                if (LE instanceof Player) {
                    try {
                        if (good) {
                            if (TDM.getTeam(((Player) LE).getName()) != T) {
                                Dirty = true;
                            } else {
                                HMLED.put(LE, event.getIntensity(LE));
                            }
                        } else {
                            if (TDM.getTeam(((Player) LE).getName()) == T) {
                                Dirty = true;
                            } else {
                                HMLED.put(LE, event.getIntensity(LE));
                            }
                        }
                    } catch (NullPointerException ex) {}
                }
            }
            if (Dirty) {
                event.setCancelled(true);
                Bukkit.getPluginManager().callEvent(new PotionSplashEvent(event.getPotion(), HMLED));
            } else if (good) {
                PE.apply(P);
            }
        } catch (NullPointerException ex) {
            event.setCancelled(true);
            P.sendMessage(ChatColor.GOLD + "You are on an invalid team.");
        }
    }
}
