package me.rrama.TDM;

import me.rrama.TDM.Commands.IntroAndRules;
import java.io.File;
import java.util.ArrayList;
import me.rrama.TDM.Achievements.Achievement;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;

public class FolderSetup {
    
    public static FileConfiguration FC;
    public static File pluginFolder, PlayersAchievementsFolder, ClassesFolder;
    
    public static void FolderSetup() {
        FC = TDM.This.getConfig();
        pluginFolder = TDM.This.getDataFolder();
        if (pluginFolder.exists() == false) {
            boolean b = pluginFolder.mkdir();
            if (b == false) {
                Bukkit.getLogger().warning((TDM.This.TagB + "DataFolder failed to be made."));
            } else {
                Bukkit.getLogger().info((TDM.This.TagB + "Created a DataFolder for you :). rrama do good?"));
            }
            FolderSetup();
        } else {
            FileSetup();
            ConfigSetup();
        }
    }
    
    public static void FileSetup() {
        boolean repeat = false;
        final String LSep = System.getProperty("line.separator");
        final String FS = System.getProperty("file.separator");
        File FileTeamDeathMatchMaps = me.rrama.RramaGaming.FolderSetup.CreateFile(pluginFolder, "TeamDeathMatchMaps.txt", TDM.This.TagB,
                "#Format: Map name | Spawns (seperated by ' ; ')" + LSep
                + "#Example: CoolMap_YEAH | 37.5, 16.0, -72.5, 0, 0 ; 28.5, 16.0, -87.5, 0, 0 ; 27.0, 19.0, -100.0, 90, 0");
        if (FileTeamDeathMatchMaps != null) {
            TDMMaps.InitializeMaps(FileTeamDeathMatchMaps);
        }

        PlayersAchievementsFolder = new File(pluginFolder, FS + "Achievements");
        if (PlayersAchievementsFolder.exists() == false) {
            boolean b = PlayersAchievementsFolder.mkdir();
            if (b == false) {
                Bukkit.getLogger().warning((TDM.This.TagB + "Folder 'Achievements' failed to be made."));
            } else {
                Bukkit.getLogger().info((TDM.This.TagB + "Created an Achievements folder for you :). rrama do good?"));
            }
            repeat = true;
        } else {
            for (Achievement A : Achievement.values()) {
                me.rrama.RramaGaming.FolderSetup.CreateFile(PlayersAchievementsFolder, A + ".txt", TDM.This.TagB, null);
            }
        }

        File FileRules = me.rrama.RramaGaming.FolderSetup.CreateFile(pluginFolder, "Rules.txt", TDM.This.TagB, "No rules have been written here yet. Ask an OP for rules.");
        if (FileRules != null) {
            IntroAndRules.ReadRulesFromFile(FileRules);
        }

        File FileIntro = me.rrama.RramaGaming.FolderSetup.CreateFile(pluginFolder, "Intro.txt", TDM.This.TagB, "No intro has been written here yet.");
        if (FileIntro != null) {
            IntroAndRules.ReadIntroFromFile(FileIntro);
        }

        File FileItemPrices = me.rrama.RramaGaming.FolderSetup.CreateFile(pluginFolder, "ItemPrices.txt", TDM.This.TagB, "1 Block of TNT: 10" + LSep + "1 Heart: 1" + LSep + "1 Care Package: 20");
        if (FileItemPrices != null) {
            Shop.ItemPricesReg(FileItemPrices);
        }
        
        ClassesFolder = new File(pluginFolder, "Classes");
        if (!ClassesFolder.exists()) {
            ClassesFolder.mkdir();
            for (int i = 0; i < 4; i++) {
                new File(ClassesFolder, String.valueOf(i)).mkdir();
            }
            me.rrama.RramaGaming.FolderSetup.CreateFile(new File(ClassesFolder, "0"), "Example.txt", TDM.This.TagB, "Default" + LSep + "UpgradeItem: 351; 13" + LSep + "Boots: 317; 1; 0" + LSep + "268; 1; 0");
        }
        for (int i = 0; i < 4; i++) {
            TDM.ClassesPriority.put(i, new ArrayList<String>());
            for (File F : new File(ClassesFolder, String.valueOf(i)).listFiles()) {
                TDM.ClassesPriority.get(i).add(new TDMClass(F).getName());
            }
        }

        if (repeat) {
            FileSetup();
        }
    }
    
    public static void ConfigSetup() {
        if (!FC.isSet("Score limit")) {
            FC.set("Score limit", 25);
        }
        if (!FC.isSet("Winning cookies")) {
            FC.set("Winning cookies", 10);
        }
        TDM.WinningCookies = FC.getInt("Winning cookies");
    }
}
