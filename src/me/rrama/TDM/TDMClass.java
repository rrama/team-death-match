package me.rrama.TDM;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import me.rrama.RramaGaming.Gamer;
import me.rrama.RramaGaming.RramaGaming;
import org.bukkit.Bukkit;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.material.MaterialData;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionType;

public class TDMClass {
    
    final String Name;
    ArrayList<String> Inheritance = new ArrayList<>();
    final int Priority; //When the class is to be called during the round, max is 3.
    ItemStack UpgradeItem, Helmet = new ItemStack(0), Chestplate = new ItemStack(0), Leggings = new ItemStack(0), Boots = new ItemStack(0);
    final ArrayList<ItemStack> Inventory = new ArrayList<>();
    
    TDMClass (final File F) {
        Name = F.getName().replaceAll(".txt", "");
        Priority = Integer.parseInt(F.getParentFile().getName());
        try (Scanner S = new Scanner(F)) {
            while (S.hasNextLine()) {
                final String ln = S.nextLine(); //Keep final so exceptions are more easily dealt with.
                try {
                    if (ln.equals("Default")) {
                        TDM.DefaultClass = Name;
                    } else if (ln.startsWith("Parents: ")) {
                        String[] ParentsS = ln.replaceFirst("Parents: ", "").split(", ");
                        for (String ParentS : ParentsS) {
                            try {
                                TDMClass Parent = TDM.Classes.get(ParentS);
                                Parent.addInheritance(Name);
                            } catch (NullPointerException ex) {
                                Bukkit.getLogger().warning((TDM.This.TagB + "Domintation Class " + Name +
                                        " has an invalid parent '" + ParentS + "'."));
                            }
                        }
                    } else if (ln.startsWith("UpgradeItem: ")) {
                        String[] lnS = ln.replaceFirst("UpgradeItem: " , "").split("; ");
                        int Item = Integer.parseInt(lnS[0]);
                        byte Data = Byte.parseByte(lnS[1]);
                        UpgradeItem = new MaterialData(Item, Data).toItemStack(1);
                        ItemMeta IM = UpgradeItem.getItemMeta();
                        IM.setDisplayName(Name);
                        UpgradeItem.setItemMeta(IM);
                    } else if (ln.startsWith("Potion: ")) {
                        String[] ItemMakerS = ln.replaceFirst("Potion: ", "").split("; ");
                        int Ammount = Integer.parseInt(ItemMakerS[0]);
                        PotionType PT = PotionType.valueOf(ItemMakerS[1].toUpperCase());
                        int amplifier = Integer.parseInt(ItemMakerS[2]);
                        boolean splash = Boolean.valueOf(ItemMakerS[3]);
                        boolean extended = Boolean.valueOf(ItemMakerS[4]);
                        Potion potion = new Potion(PT, amplifier);
                        potion.setSplash(splash);
                        if (extended) potion.setHasExtendedDuration(true);
                        Inventory.add(potion.toItemStack(Ammount));
                    } else {
                        String Armour = "", ln2 = ln;
                        if (ln2.contains(": ")) {
                            String[] Break = ln2.split(": ");
                            Armour = Break[0].toUpperCase();
                            ln2 = Break[1];
                        }
                        String[] ItemMakerS = ln2.split("; ");
                        int Item = Integer.parseInt(ItemMakerS[0]);
                        int Ammount = Integer.parseInt(ItemMakerS[1]);
                        short Damage = Short.parseShort(ItemMakerS[2]);
                        ItemStack iss = new ItemStack(Item, Ammount, Damage);
                        
                        String DataS = ItemMakerS[3];
                        while (!DataS.equals("0")) { //Data dealwithings.
                            if (DataS.toUpperCase().startsWith("ENCHANTMENTS(")) {
                                String[] DataSs = DataS.replaceFirst("ENCHANTMENTS(", "").split(")");
                                DataS = DataSs[1];
                                String[] Enchants = DataSs[0].split("'");
                                for (String EnchantS : Enchants) {
                                    String[] EnchantAndMul = EnchantS.split("*");
                                    Enchantment E = Enchantment.getByName(EnchantAndMul[0].toUpperCase());
                                    int Mul = Integer.valueOf(EnchantAndMul[1]);
                                    iss.addEnchantment(E, Mul);
                                }
                            } else if (DataS.toUpperCase().startsWith("NAME(")) {
                                String[] DataSs = DataS.replaceFirst("NAME(", "").split(")");
                                DataS = DataSs[1];
                                String issName = DataSs[0];
                                ItemMeta IM = iss.getItemMeta();
                                IM.setDisplayName(issName);
                                iss.setItemMeta(IM);
                            } else if (DataS.toUpperCase().startsWith("LORE(")) {
                                String[] DataSs = DataS.replaceFirst("LORE(", "").split(")");
                                DataS = DataSs[1];
                                List<String> issLore = Arrays.asList(DataSs[0].split(" %n% "));
                                ItemMeta IM = iss.getItemMeta();
                                IM.setLore(issLore);
                                iss.setItemMeta(IM);
                            } else {
                                try {
                                    byte Data = Byte.parseByte(ItemMakerS[3]);
                                    iss = new ItemStack(Item, Ammount, Damage);
                                    iss.setData(new MaterialData(Item, Data));
                                } catch (NumberFormatException ex) {
                                    Bukkit.getLogger().warning((TDM.This.TagB + "Class file '" + Name +
                                            "' has an error on line '" + ln + "' in data '" + DataS + "' " + ex.getLocalizedMessage() + "."));
                                    break;
                                }
                            }
                        }
                        
                        switch (Armour) {
                            case "HELMET":
                                Helmet = iss;
                                break;
                            case "CHESTPLATE":
                                Chestplate = iss;
                                break;
                            case "LEGGINGS":
                                Leggings = iss;
                                break;
                            case "BOOTS":
                                Boots = iss;
                                break;
                            default:
                                Inventory.add(iss);
                                break;
                        }
                    }
                } catch (ArrayIndexOutOfBoundsException | IllegalArgumentException ex) {
                    Bukkit.getLogger().warning((TDM.This.TagB + "Class file '" + Name +
                            "' has an error on line '" + ln + "' " + ex.getLocalizedMessage() + "."));
                }
            }
            if (UpgradeItem == null) {
                if (!TDM.DefaultClass.equals(Name)) {
                    Bukkit.getLogger().warning(("No UpgradeItem assigned for class " + Name));
                }
            } else {
                TDM.ClassesUpgradeItem.put(UpgradeItem.getData(), Name);
            }
            TDM.Classes.put(Name, this);
        } catch (FileNotFoundException ex) {
            Bukkit.getLogger().warning((TDM.This.TagB + Name + " not found, even though it is in the Classes folder."));
        }
    }
    
    public String getName() {
        return Name;
    }
    
    public ItemStack getUpgradeItem() {
        return UpgradeItem;
    }
    
    public int getPriority() {
        return Priority;
    }
    
    public List<ItemStack> getArmour() {
        return Arrays.asList(Helmet, Chestplate, Leggings, Boots);
    }
    
    public ArrayList<ItemStack> getInventory() {
        return Inventory;
    }
    
    public ArrayList<String> getInheritance() {
        return Inheritance;
    }
    
    public boolean addInheritance(final String Child) {
        return Inheritance.add(Child);
    }
    
    public void setPlayerInvertory(final Player P) {
        PlayerInventory PI = P.getInventory();
        PI.clear();
        PI.setHelmet(Helmet);
        PI.setChestplate(Chestplate);
        PI.setLeggings(Leggings);
        PI.setBoots(Boots);
        for (ItemStack iss : Inventory) {
            PI.addItem(iss);
        }
        Gamer G = RramaGaming.getGamer(P.getName());
        if (((int)G.getMetaData("TeamDeathMatch-Upgrade")) < TimedTasks.stage && TimedTasks.stage != 0) {
            for (String InheritanceS : TDM.Classes.get((String)G.getMetaData("TeamDeathMatch-Class")).getInheritance()) {
                PI.addItem(TDM.Classes.get(InheritanceS).getUpgradeItem());
            }
        }
    }
}