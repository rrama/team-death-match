package me.rrama.TDM;

import me.rrama.RramaGaming.Gamer;
import me.rrama.RramaGaming.RoundState;
import me.rrama.RramaGaming.RramaGaming;
import me.rrama.RramaGaming.Timer;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitTask;

public class TimedTasks {
    
    public static int stage;
    public static BukkitTask TimeTask, RegenTask;
    
    public static void TimerMain() {
        if (RramaGaming.gameInPlay.equals("TeamDeathMatch") && RramaGaming.R == RoundState.InGame) {
            stage = 0;
            new Timer(TDM.This, false, 485);
            Time();
            CallRegen();
        } else {
            Bukkit.broadcastMessage(TDM.This.TagB + "TimerMain() was launched when TeamDeathMatch was not in play or R != InGame");
        }
    }
    
    public static void Time() {
        TimeTask = Bukkit.getScheduler().runTaskTimerAsynchronously(TDM.This, new Runnable() {

            @Override
            public void run() {
                if (RramaGaming.gameInPlay.equals("TeamDeathMatch") && RramaGaming.R == RoundState.InGame && TDM.This.timer.isRunning()) {
                    stage++;
                    for (Gamer G : RramaGaming.getGamers()) {
                        if (G.m()) {
                            for (String InheritanceS : TDM.Classes.get((String)G.getMetaData("TeamDeathMatch-Class")).getInheritance()) {
                                G.getPlayer().getInventory().addItem(TDM.Classes.get(InheritanceS).getUpgradeItem());
                            }
                        }
                    }
                } else {
                    TimeTask.cancel();
                }
            }
        }, 2405, 2400);
    }
    
    public static void CallRegen() {
        RegenTask = Bukkit.getScheduler().runTaskTimer(TDM.This, new Runnable() {

            @Override
            public void run() {
                if (TDM.This.timer.isRunning()) {
                    for (Gamer G : RramaGaming.getGamers()) {
                        if (G.m()) {
                            Player P = G.getPlayer();
                            if (G.isAlive()) {
                                try {
                                    P.setHealth(P.getHealth()+1);
                                } catch (IllegalArgumentException ex) {}
                            }
                        }
                    }
                } else {
                    RegenTask.cancel();
                }
            }
            
        }, 40, 40);
    }
}