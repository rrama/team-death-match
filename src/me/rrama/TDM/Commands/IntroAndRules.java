package me.rrama.TDM.Commands;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
import me.rrama.TDM.TDM;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class IntroAndRules implements CommandExecutor { //ToDo: Allow sending to someone else if you have permissions.
    
    public static ArrayList<String> Rules = new ArrayList<>();
    public static ArrayList<String> Intro = new ArrayList<>();
    
    public static void ReadRulesFromFile(final File FileRules) {
        try {
            Scanner s = new Scanner(FileRules);
            s.reset();
            while (s.hasNextLine()) {
                Rules.add(s.nextLine());
            }
        } catch (FileNotFoundException ex) {
            Bukkit.getLogger().warning((TDM.This.TagB + "Rules.txt not found."));
        }
    }
    
    public static void ReadIntroFromFile(final File FileIntro) {
        try {
            Scanner s = new Scanner(FileIntro);
            s.reset();
            while (s.hasNextLine()) {
                Intro.add(s.nextLine());
            }
        } catch (FileNotFoundException ex) {
            Bukkit.getLogger().warning((TDM.This.TagB + "Intro.txt not found."));
        }
    }
    
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLable, String[] args) {
        if (commandLable.equalsIgnoreCase("TeamDeathMatchIntro") || commandLable.equalsIgnoreCase("TDMIntro")) {
            sender.sendMessage(ChatColor.YELLOW + "--- Team Death Match Intro ---");
            for (String s : Intro) {
                sender.sendMessage(ChatColor.YELLOW + s);
            }
            return true;
        } else if (commandLable.equalsIgnoreCase("TeamDeathMatchRules") || commandLable.equalsIgnoreCase("TDMRules")) {
            sender.sendMessage(ChatColor.YELLOW + "--- Team Death Match Rules ---");
            for (String s : Rules) {
                sender.sendMessage(ChatColor.YELLOW + s);
            }
            return true;
        } else {
            return false;
        }
    }
}