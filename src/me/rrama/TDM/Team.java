package me.rrama.TDM;

import java.util.ArrayList;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.map.MapCursor;

public class Team {
    
    final public ArrayList<String> Members;
    final public ArrayList<String> BetratyingMembers = new ArrayList<>();
    private int Score = 0;
    final private String Name;
    final private ChatColor TeamColour;
    final private MapCursor.Type PointerColour;
    
    public Team(final String Name, final ChatColor TeamColour, final MapCursor.Type PointerColour, final ArrayList<String> Members) {
        this.Name = Name;
        this.TeamColour = TeamColour;
        this.PointerColour = PointerColour;
        this.Members = Members;
    }
    
    public int getScore() {
        return Score;
    }
    
    public void setScore(final int Score) {
        this.Score = Score;
    }
    
    public void ScoreAdd1() {
        ++Score;
    }
    
    public void CheckScoreLimit() {
        if (Score >= TDM.ScoreLimit) {
            TDM.This.callEndRound(false);
        }
    }
    
    public String getName() {
        return Name;
    }
    
    public ChatColor getTeamColour() {
        return TeamColour;
    }
    
    public MapCursor.Type getPointerColour() {
        return PointerColour;
    }
    
    public void sendMessage(String message) {
        for (String s : Members) {
            Bukkit.getPlayerExact(s).sendMessage(message);
        }
    }
}
