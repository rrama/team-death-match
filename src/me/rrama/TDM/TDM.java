package me.rrama.TDM;

/**
 *
 * @author rrama
 * All rights reserved to rrama.
 * No copying/stealing any part of the code (Exceptions; You are from the Bukkit team, you have written consent from rrama).
 * No copying/stealing ideas from the code (Exceptions; You are from the Bukkit team, you have written consent from rrama).
 * 
 * @Credits credit goes to rrama (author)
 */

import me.rrama.TDM.Commands.AchievementCommands;
import me.rrama.TDM.Commands.IntroAndRules;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import me.rrama.TDM.Events.RegisterEvents;
import me.rrama.RramaGaming.CookieFileException;
import me.rrama.RramaGaming.Gamer;
import me.rrama.RramaGaming.GamingPlugin;
import me.rrama.RramaGaming.MaxHeight;
import me.rrama.RramaGaming.RramaGaming;
import me.rrama.TDM.Events.DeathAndRespawnEvent;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.map.MapCursor;
import org.bukkit.material.MaterialData;

public class TDM extends GamingPlugin {
    
    public static String DefaultClass;
    public static final HashMap<String, TDMClass> Classes = new HashMap<>();
    public static final HashMap<MaterialData, String> ClassesUpgradeItem = new HashMap<>();
    public static final HashMap<Integer, ArrayList<String>> ClassesPriority = new HashMap<>();
    
    public static int WinningCookies, ScoreLimit;
    public static GamingPlugin This;
    public static Team Red, Blue;
    
    @Override
    public void onGameDisable() {
        Bukkit.getScheduler().cancelTasks(This);
    }
    
    @Override
    public void onGameEnable() {
        This = this;
        MinimumPlayersNeeded = 2;
        ForceStart = false;
        FolderSetup.FolderSetup();
        
        RegisterEvents.Register();
        
        getCommand("TeamDeathMatchIntro").setExecutor(new IntroAndRules());
        getCommand("TeamDeathMatchRules").setExecutor(new IntroAndRules());
        getCommand("TeamDeathMatchAchievements").setExecutor(new AchievementCommands());
    }
    
    @Override
    public void endRound(boolean FinalStop) {
        Team WinningTeam;
        if (Red.getScore() > Blue.getScore()) {
            WinningTeam = Red;
            Achievements.AwardAchievement(Blue.Members, Achievements.Achievement.Loser);
        } else if (Blue.getScore() > Red.getScore()) {
            WinningTeam = Blue;
            Achievements.AwardAchievement(Red.Members, Achievements.Achievement.Loser);
        } else {
            Bukkit.broadcastMessage(This.TagB +  "Round ended. It's a draw, stand down.");
            ArrayList<String> Both = new ArrayList<>();
            Both.addAll(Red.Members);
            Both.addAll(Blue.Members);
            Achievements.AwardAchievement(Both, Achievements.Achievement.Stuck_In_The_Middle);
            return;
        }
        Achievements.AwardAchievement(WinningTeam.Members, Achievements.Achievement.Winner);
        Bukkit.broadcastMessage(This.TagB + "Round ended. " + WinningTeam.getTeamColour() + WinningTeam.getName() + " Team" + ChatColor.BLUE + " won!");
        Bukkit.broadcastMessage(This.TagB + "Scores: " + ChatColor.DARK_RED + "Red got " + Red.getScore() + ChatColor.BLUE + " and " + ChatColor.DARK_BLUE + "Blue got " + Blue.getScore());
        for (String s : WinningTeam.Members) {
            try {
                RramaGaming.getGamer(s).addCookies(WinningCookies);
                Bukkit.getPlayerExact(s).sendMessage(ChatColor.GOLD + "You got " + WinningCookies + " cookies for being a champ :)");
            } catch (CookieFileException ex) {
                ex.sendPlayerMessage();
                ex.sendConsoleMessage();
            }
        }
    }
    
    @Override
    public void startPreGame() {
        This.callStartRound();
    }
    
    @Override
    public void startRound() {
        MaxHeight.BuildHeight = -1337;
        RramaGaming.WasInRound.clear();
        TimedTasks.TimerMain();
        TDMClass DC = TDM.Classes.get(DefaultClass);
        for (Gamer G : RramaGaming.getGamers()) {
            if (G.m()) {
                Player p = G.getPlayer();
                RramaGaming.WasInRound.add(p.getName());
                p.setHealth(p.getMaxHealth());
                p.setGameMode(GameMode.ADVENTURE);
                G.addMetaData("TeamDeathMatch-Class", DefaultClass);
                G.addMetaData("TeamDeathMatch-Upgrade", 0);
                DC.setPlayerInvertory(p);
            }
        }
        SetUpTeams();
        TeleportToStartSpawns();
        Bukkit.broadcastMessage(This.TagB + "Round has started!");
    }
    
    public static void SetUpTeams() {
        ArrayList<String> SortingHat = new ArrayList<>();
        for (Gamer G : RramaGaming.getGamers()) {
            if (G.m() && !G.isRef()) {
                SortingHat.add(G.getName());
            }
        }
        Collections.shuffle(SortingHat);
        ArrayList<String> Reds = new ArrayList<>();
        ArrayList<String> Blues = new ArrayList<>();
        for (int i = 0; i <= SortingHat.size() - 1; i++) {
            String Muggle = SortingHat.get(i);
            Player MudBlood = Bukkit.getPlayerExact(Muggle);
            MudBlood.setTotalExperience(0);
            if (i % 2 == 0) {
                Reds.add(Muggle);
                MudBlood.sendMessage(ChatColor.GOLD + "You are in team " + ChatColor.DARK_RED + "Red" + ChatColor.GOLD + ".");
                RramaGaming.getGamer(Muggle).setTagColour(ChatColor.DARK_RED);
            } else {
                Blues.add(Muggle);
                MudBlood.sendMessage(ChatColor.GOLD + "You are in team " + ChatColor.DARK_BLUE + "Blue" + ChatColor.GOLD + ".");
                RramaGaming.getGamer(Muggle).setTagColour(ChatColor.DARK_BLUE);
            }
        }
        Red = new Team("Red", ChatColor.DARK_RED, MapCursor.Type.RED_POINTER, Reds);
        Blue = new Team("Blue", ChatColor.DARK_BLUE, MapCursor.Type.BLUE_POINTER, Blues);
    }
    
    private static void TeleportToStartSpawns() {
        Location RedStart = DeathAndRespawnEvent.Spawning(), BlueStart = DeathAndRespawnEvent.Spawning();
        while (RedStart.equals(BlueStart)) {
            BlueStart = DeathAndRespawnEvent.Spawning();
        }
        for (String S : TDM.Red.Members) {
            Bukkit.getPlayer(S).teleport(RedStart);
        }
        for (String S : TDM.Blue.Members) {
            Bukkit.getPlayer(S).teleport(BlueStart);
        }
    }
    
    public static Team getTeam(String PN) {
        if (Red.Members.contains(PN) || Red.BetratyingMembers.contains(PN)) return Red;
        else if (Blue.Members.contains(PN) || Blue.BetratyingMembers.contains(PN)) return Blue;
        return null;
    }
}
