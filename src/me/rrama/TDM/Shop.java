package me.rrama.TDM;

import me.rrama.TDM.ShopItems.Cookie;
import me.rrama.TDM.ShopItems.TNT;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import me.rrama.RramaGaming.ShopItems.ShopCarePackage;
import org.bukkit.Bukkit;

public class Shop {
    
    public static void ItemPricesReg(final File FileItemPrices) {
        try {
            Scanner s = new Scanner(FileItemPrices);
            s.reset();
            int count = 5;
            while (s.hasNextLine()) {
                String S = s.nextLine();
                String[] Sy = S.split(": ");
                if (Sy.length != 2) {
                    Bukkit.getLogger().warning((TDM.This.TagB + "The line '" + S + "' does not follow the correct format for 'ItemPrices.txt'."));
                } else if (!Sy[1].matches("-?\\d+(.\\d+)?")) {
                    Bukkit.getLogger().warning((TDM.This.TagB + "'" + Sy[1] + "' in '" + S + "' in 'ItemPrices.txt' is not an integer."));
                } else {
                    String Name = Sy[0];
                    int ItemPrice = Integer.parseInt(Sy[1]);
                    //IndiItems reg
                    if (count == 5) {
                        new TNT(TDM.This, Name, ItemPrice);
                    } else if (count == 6) {
                        new Cookie(TDM.This, Name, ItemPrice);
                    } else if (count == 7) {
                        new ShopCarePackage(TDM.This, Name, ItemPrice);
                    }
                    //IndiItems reg end
                }
                count++;
            }
        } catch (FileNotFoundException ex) {
            Bukkit.getLogger().warning((TDM.This.TagB + "Could not find File 'ItemPrices.txt' even though it exists."));
        }
    }
}