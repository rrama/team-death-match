package me.rrama.TDM;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
import me.rrama.RramaGaming.*;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;

public class TDMMaps {
    
    public static void InitializeMaps(final File F) {
        try (Scanner S = new Scanner(F)) {
            while (S.hasNextLine()) {
                ArrayList<Location> Spawns = new ArrayList<>();
                String LS = S.nextLine();
                World W = Bukkit.getWorlds().get(0);
                if (!LS.startsWith("#")) {
                    final String[] LSA = LS.split(" \\| ");
                    TDM.This.Maps.add(LSA[0]);
                    GamingMap ThisMap = GamingMaps.GamingMaps.get(LSA[0]);
                    for (String SpawnsS : LSA[1].split(" ; ")) {
                        String[] LCS = SpawnsS.split(", ");
                        Location LC = new Location(W, Double.valueOf(LCS[0]), Double.valueOf(LCS[1]), Double.valueOf(LCS[2]),
                                Integer.valueOf(LCS[3]), Integer.valueOf(LCS[4]));
                        Spawns.add(LC);
                    }
                    ThisMap.addMetaData("TeamDeathMatch-Spawns", Spawns);
                }
            }
            if (TDM.This.Maps.isEmpty()) {
                Bukkit.getLogger().warning((TDM.This.TagB + "No mapz in da TeamDeathMatchMaps file!!! TeamDeathMatch plugin die now x_x ided."));
                Bukkit.getPluginManager().disablePlugin(TDM.This);
            }
        } catch (FileNotFoundException ex) {
            Bukkit.getLogger().warning((TDM.This.TagB + "Cannot find da TeamDeathMatchMaps file!!! TeamDeathMatch plugin die now x_x ided."));
            Bukkit.getPluginManager().disablePlugin(TDM.This);
        }
    }
}